﻿using System;
using System.IO;
using NSwag;
using NSwag.CodeGeneration.TypeScript;

namespace nswag_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            var document = SwaggerDocument.FromUrlAsync("http://139.59.92.54:5000/swagger/v1/swagger.json");
            var settings = new SwaggerToTypeScriptClientGeneratorSettings {
                ClassName = "{controller}Client"
            };
            var generator = new SwaggerToTypeScriptClientGenerator(document.Result, settings);
            var code = generator.GenerateFile();
            File.WriteAllText("Demo.ts", code);
        }
    }
}
